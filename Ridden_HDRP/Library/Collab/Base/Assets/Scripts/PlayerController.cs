﻿using UnityEngine;
using UnityEngine.AI;

public class PlayerController : MonoBehaviour
{
    //public float dashDistance = 20f;
    public Camera cam;

    ///////////////////////////

    private float moveH, moveV;
    private float iFrameTimer;
    private float dashTimer;

    private NavMeshAgent nma;
    private Rigidbody rb;

    private Ray mouseRay;

    //private StatScript stats;

    private bool dash = true;
    private bool iFrame = false;

    void Start()
    {
        nma = GetComponent<NavMeshAgent>();
        //stats = GetComponent<StatScript>();
        rb = GetComponent<Rigidbody>();
    }

    private void FixedUpdate()  
    {
        moveH = Input.GetAxis("Horizontal") * nma.speed;
        moveV = Input.GetAxis("Vertical") * nma.speed;
        Vector3 velocity = new Vector3(moveH, 0, moveV);        // Create a vector3 using Unity's Horizontal/Vertical axis

        mouseRay = cam.ScreenPointToRay(Input.mousePosition);       // Acquire mouseposition and store in Ray data
        Plane groundPlane = new Plane(Vector3.up, Vector3.zero);        // Create a blank plane facing up (for raycasting purposes)
        float rayLength;

        if(groundPlane.Raycast(mouseRay, out rayLength))        // If the mouseray hits the created plane, make the player look in the direction it casts
        {
            Vector3 pointToLook = mouseRay.GetPoint(rayLength);
            Debug.DrawLine(mouseRay.origin, pointToLook, Color.blue);

            transform.LookAt(pointToLook);
        }

        if(Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.D))
        {
            nma.velocity = velocity;
            //GetComponent<Animator>().SetBool("Running", true);

            if (Input.GetKeyDown(KeyCode.Space))        // Initiate dash
            {
                if (dash)       // If the dash boolean is true, translate your current position by your velocity in the moving direction
                {
                    print(nma.transform.position);

                    nma.transform.Translate(velocity * .75f, Space.World);
                    dash = false;
                    iFrame = true;      // Activate/Deactivate any required data (such as dash for cooldown and iFrame bool for its respective function)

                    print(nma.transform.position);
                }
            }
        }
        else
        {
            //GetComponent<Animator>().SetBool("Running", false);
        }

        if (iFrame)     // If the iFrame boolean is true, turn off collision detection for a fixed amount of time
        {
            iFrameTimer += Time.deltaTime;

            if (iFrameTimer < .1f)
            {
                rb.detectCollisions = false;
            }
            else
            {
                rb.detectCollisions = true;
                iFrame = false;
                iFrameTimer = 0f;
            }
        }

        if (!dash)      // If the dash boolean is set to false, start cooldown timer
        {
            dashTimer += Time.deltaTime;

            if (dashTimer > 2f)     // If the cooldown timer is above the required time, set the dash boolean to true
            {
                dash = true;
                dashTimer = 0f;
            }
        }

        if (Input.GetMouseButtonDown(0))
        {
            //stats.Attack();
            //GetComponent<Animator>().SetBool("Attacking", true);
        }
        else
        {
            //GetComponent<Animator>().SetBool("Attacking", false);
        }
    }
}