﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    [Header("Spawner Settings")]
    [Tooltip("Is the spawner allowed to currently spawn?")]
    public bool active;
    [Tooltip("Within what radius of the player should the spawner become active? Should be higher than Min.")]
    public float activeDistanceMax;
    [Tooltip("Within what radius of the player should the spawner become inactive? Should be lower than Max.")]
    public float activeDistanceMin;
    [Tooltip("How often (in seconds) the spawner should spawn something.")]
    public float interval;
    [Tooltip("The parts per total that nothing will spawn")]
    public float ratioNothing;
    [Tooltip("How far is the spawner currently from the player?")]
    public float distanceToPlayer;
    [Tooltip("The player game object. Will attempt to auto assign if empty.")]
    public GameObject player;
    [Tooltip("Automatically spawned enemies will be grouped as children of this object. Will attempt to auto assign.")]
    public GameObject autoGroup;

    [Header("Ridden Settings")]
    [Tooltip("Is the spawning of Ridden enabled?")]
    public bool spawnRidden;
    [Tooltip("The parts per total that a Ridden will spawn")]
    public float ratioRidden;
    [Tooltip("The Ridden game object. Will not spawn if empty.")]
    public GameObject prefabRidden;

    [Header("Plague Doctor Settings")]
    [Tooltip("Is the spawning of Plague Doctors enabled?")]
    public bool spawnPlagueDoctor;
    [Tooltip("The parts per total that a Plague Doctor will spawn")]
    public float ratioPlagueDoctor;
    [Tooltip("The Plague Doctor game object. Will not spawn if empty.")]
    public GameObject prefabPlagueDoctor;

    [Header("Global Stat Script")]
    public GameObject GlobalStatHolder;
    private GlobalStats GlobalStats;

    // Start is called before the first frame update
    void Start()
    {
        //Assign the global stat script as a referencable item
        if (GameObject.FindGameObjectWithTag("GlobalStats") != null)
        {
            GlobalStatHolder = GameObject.FindGameObjectWithTag("GlobalStats");
            GlobalStats = GlobalStatHolder.GetComponent<GlobalStats>();
        }
        else
        {
            print("ERROR: Global Stat Script not assigned. Stats in this component will not be logged globally.");
        }
        
        if (player == null)
        {
            print("Player GameObject not assigned to spawner '" + name + "'. Attempting to find GameObject with tag 'Player'.");
            player = GameObject.FindGameObjectWithTag("Player");
            if (player == null)
            {
                print("Player GameObject not found. Spawner '" + name + "' will be disabled.");
                gameObject.SetActive(false);
                return;
            } 
            else
            {
                print("GameObject with tag 'Player' found. Spawner '" + name + "' will use found GameObject '" + player.name + "'.");
            }
        }
        if (autoGroup == null)
        {
            if (GameObject.FindGameObjectWithTag("AutoEnemyHolder") != null)
            {
                autoGroup = GameObject.FindGameObjectWithTag("AutoEnemyHolder");
            }
        }

        StartCoroutine(DelayedStart());
    }

    // Update is called once per frame
    void Update()
    {
        if (player == null)
        {
            print("Player GameObject reference lost. Spawner '" + name + "' will disable.");
            StopCoroutine(DelayedStart());
            StopCoroutine(Spawning());
            gameObject.SetActive(false);
            return;
        }

        distanceToPlayer = Vector3.Distance(gameObject.transform.position, player.transform.position);

        if (activeDistanceMin > activeDistanceMax)
        {
            activeDistanceMin = activeDistanceMax;
        }
        if ((distanceToPlayer < activeDistanceMax) && (distanceToPlayer > activeDistanceMin))        
        {
            active = true;
        }
        else
        {
            active = false;
        }
    }

    IEnumerator Spawning()
    {
        while (true)
        {
            if (active)
            {
                float totalRatio = ratioNothing;     //find the total number of items it could spawn as
                if (spawnRidden)
                {
                    totalRatio += ratioRidden;
                }
                if (spawnPlagueDoctor)
                {
                    totalRatio += ratioPlagueDoctor;
                }

                float chanceRidden = 0;
                if (spawnRidden)
                {
                    chanceRidden += (100f / totalRatio) * ratioRidden;     //find the chance that it will be a ridden out of 100
                }
                float chancePlagueDoctor = 0;
                if (spawnPlagueDoctor)
                {
                    chancePlagueDoctor += ((100f / totalRatio) * ratioPlagueDoctor) + chanceRidden;        //find the chance it will be a doctor, then add the ridden chance to it
                }

                float roll = Random.Range(0f, 100);     //roll a random number between 0 - 100
                
                if (roll < chanceRidden)        //if the rolled number is within the ridden spawn chance range, spawn ridden
                {
                    print(name + " spawning Ridden.");
                    Spawn(prefabRidden);
                }
                else if (roll < chancePlagueDoctor)     //if a ridden wasn't spawned, check if the remaining chance was with the chance of a doctor spawning
                {
                    print(name + " spawning Plague Doctor.");
                    Spawn(prefabPlagueDoctor);
                }
                else        //if roll was not in range for either, spawn nothing.
                {
                    print(name + " spawning nothing.");
                }

                yield return new WaitForSeconds(interval);
            }
        }
    }

    IEnumerator DelayedStart()
    {
        yield return new WaitForSeconds(Random.Range(0f, interval));
        print (name + " Started Spawning.");
        StartCoroutine(Spawning());
    }

    bool Spawn(GameObject spawn)
    {
        if (spawn == null)
        {
            print ("'" + name + "' told to spawn an unassigned GameObject. Spawn failed.");
            return false;
        }
        GameObject spawned = Instantiate(spawn, transform.position, transform.rotation);
        spawned.transform.LookAt(player.transform);
        spawned.transform.parent = autoGroup.transform;
        return true;        
    }
}
