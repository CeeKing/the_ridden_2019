﻿using UnityEngine;
using UnityEngine.AI;


public class AttackAction : Action
{
    private UnityEngine.GameObject player;
    private float Distance;
    private float attackTimer;

    bool innerCheck;
    bool outerCheck;
    public override float Evaluate(Agent agent)     // Static float data
    {
        return .5f;
    }

    public override void UpdateAction(Agent agent)      // Check inner/outer radius to determine which attack to output
    {
        agent.UpdateRotation();     // Turn off the NavMeshAgent in-built rotatin
        Distance = Vector3.Distance(agent.player.transform.position, transform.position);

        attackTimer += Time.deltaTime;

        //GetComponent<Animator>().SetFloat("Melee Timer", attackTimer);

        if (Distance <= agent.innerRadius)      // If the target is inside the inner radius, check for GameObject tags and check booleans
        {

            if (!innerCheck)     // If the inner radius check was false, update information and change check booleans
            {
                print("innerRadius Breached");
                innerCheck = true;
                outerCheck = false;
            }

            if(attackTimer >= 2.5f)     // If the attack timer is greater than (or equal) to determined time, attack accordingly
            {
                if(agent.tag == "PlagueDoctor")     // If the agent is tagged as a "PlagueDoctor", activate his melee attack, else ridden attack
                {
                    // Melee Attack
                    print("Plague Melee");
                    agent.fAttack.Attack();
                    attackTimer = 0f;
                }
                else
                {
                    // Ridden Melee
                    print("Ridden Melee");
                    agent.fAttack.Attack();
                    attackTimer = 0f;
                }
            }
        }
        else if(Distance <= agent.outerRadius && Distance > agent.innerRadius)      // If the target is inside the outer radius, and outside the inner radius, check for GameObject tags and check booleans
        {

            if (!outerCheck)     // If the outer radius check was false, update information and change check booleans
            {
                print("innerRadius Secure");
                outerCheck = true;
                innerCheck = false;
            }

            if(attackTimer >= 2.5f)     // If the attack timer is greater than (or equal) to determined time, attack accordingly
            {
                if(agent.tag == "PlagueDoctor")     // If the agent is tagged as a "PlagueDoctor", activate his ranged attack, else ridden attack
                {
                    // Ranged attack
                    print("Plague Ranged");
                    agent.fAttack.Attack();
                    attackTimer = 0f;
                }
                else
                {
                    // Ridden Melee
                    print("Ridden Melee");
                    agent.fAttack.Attack();
                    attackTimer = 0f;
                }
            }
        }
    }

    public override void Enter(Agent agent)     // Acquire/set relative information/data
    {
        player = UnityEngine.GameObject.FindWithTag("Player");
        //GetComponent<Animator>().SetBool("Melee", true);
        attackTimer = 2f;
        innerCheck = false;
        outerCheck = false;
    }

    public override void Exit(Agent agent)
    {
        //GetComponent<Animator>().SetBool("Melee", false);
    }
}