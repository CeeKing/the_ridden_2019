﻿using UnityEngine.AI;
using UnityEngine;

public class SeekAction : Action
{
    private float Distance;
    public override float Evaluate(Agent agent)     // Check the agent's position relative to the players, and output relative float data
    {
        Distance = Vector3.Distance(agent.player.transform.position, transform.position);

        if(Distance <= agent.outerRadius)
        {
            return 0f;
        }
        else
        {
            return 1f;
        }
    }

    public override void UpdateAction(Agent agent)      // Set the agent to begin seeking the player
    {
        agent.nma.SetDestination(agent.player.transform.position);
    }

    public override void Enter(Agent agent)     // Start the NavMeshAgent
    {
        agent.nma.isStopped = false;
        //agent.GetComponent<Animator>().SetBool("Seeking", true);
        agent.nma.avoidancePriority = Random.Range(1, 50);
    }

    public override void Exit(Agent agent)      // Stop the NavMeshAgent
    {
        agent.nma.isStopped = true;
        //agent.GetComponent<Animator>().SetBool("Seeking", false);
    }
}
