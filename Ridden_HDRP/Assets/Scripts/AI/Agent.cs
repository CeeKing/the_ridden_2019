﻿using UnityEngine.AI;
using UnityEngine;

public class Agent : MonoBehaviour
{
    [Range(0, 10)]
    public float outerRadius;
    public float rotSpeed;

    Action[] actions;       // Array of available actions
    public Action currentAction;        // The action we are currently carrying out

    [HideInInspector]
    public UnityEngine.GameObject player;

    [HideInInspector]
    public float innerRadius;

    [HideInInspector]
    public NavMeshAgent nma;

    [HideInInspector]
    public StatScript stats;
    public FunctionAttack fAttack;

    private Action bestAction;
    private void Start()        // Used for initialization
    {
        actions = GetComponents<Action>();      // Get all action-derived classes that are siblings of us
        player = UnityEngine.GameObject.FindWithTag("Player");

        if (GetComponent<NavMeshAgent>() != null)
        {
            nma = GetComponent<NavMeshAgent>();
        }
        else
        {
            //print error
        }

        stats = GetComponent<StatScript>();
        fAttack = GetComponent<FunctionAttack>();
    }

    private void Update()       // Update is called once per frame
    {
        if(nma.tag == "PlagueDoctor")
        {
            innerRadius = outerRadius / 2;
        }
        else
        {
            innerRadius = 0f;
        }

        if(player.GetComponent<Health>().healthCurrent > 0f)
        {
            bestAction = GetBestAction();      // Find the best action each frame

            if (bestAction != currentAction)      // If it's different from what we were doing, switch the FSM
            {
                if(currentAction)
                {
                    currentAction.Exit(this);
                }

                currentAction = bestAction;

                if(currentAction)
                {
                    currentAction.Enter(this);
                }
            }

            if (currentAction)      // Update the current action
            {
                currentAction.UpdateAction(this);
            }

            Action GetBestAction()      // Determine the best action based off of numerical values
            {
                Action action = null;
                float bestValue = 0;

                foreach(Action a in actions)
                {
                    float value = a.Evaluate(this);
                    if(action == null || value > bestValue)
                    {
                        action = a;
                        bestValue = value;
                    }
                }

                return action;
            }
        }
        else
        {
            bestAction = null;
            nma.isStopped = true;
        }
    }

    private void OnDrawGizmosSelected()     // Draw wire radius (DEBUGGING PURPOSES)
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, outerRadius);      // Draw the outer radius

        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(transform.position, innerRadius);     // Draw the inner radius
    }

    public void UpdateRotation()        // Update the rotation of the agent to face the player
    {
        if(bestAction != null)
        {
            UnityEngine.GameObject player = UnityEngine.GameObject.FindWithTag("Player");
            Vector3 direction = (player.transform.position - transform.position).normalized;
            Quaternion lookRot = Quaternion.LookRotation(direction);
            transform.rotation = Quaternion.Slerp(transform.rotation, lookRot, Time.deltaTime * rotSpeed);
        }
    }
}

public abstract class Action : MonoBehaviour        // Abstract class for FSM
{
    public abstract float Evaluate(Agent agent);
    public abstract void UpdateAction(Agent agent);
    public abstract void Enter(Agent agent);
    public abstract void Exit(Agent agent);
}