﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour
{
    [Header ("Stats")]  //basic stats of all characters using this script
    public float healthCurrent;
    public float healthMax;
    
    [Header("Function Scripts")]
    [Tooltip("Leave blank if not dropping anything")]
    public FunctionDropItem fDropItem;
    public FunctionLayerAndTag fLAT;

    [Header("Global Stat Script")]
    public GameObject GlobalStatHolder;
    private GlobalStats GlobalStats;

    // Start is called before the first frame update
    void Start()
    {
        //Assign the global stat script as a referencable item
        if (GameObject.FindGameObjectWithTag("GlobalStats") != null)
        {
            GlobalStatHolder = GameObject.FindGameObjectWithTag("GlobalStats");
            GlobalStats = GlobalStatHolder.GetComponent<GlobalStats>();
        }
        else
        {
            print("ERROR: Global Stat Script not assigned. Stats in this component will not be logged globally.");
        }

        if (gameObject.GetComponent<FunctionDropItem>() != null)
        {
            fDropItem = gameObject.GetComponent<FunctionDropItem>();
        }
        if (gameObject.GetComponent<FunctionLayerAndTag>() != null)
        {
            fLAT = gameObject.GetComponent<FunctionLayerAndTag>();
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (healthCurrent > healthMax)
        {
            #region Global Stats 
            GlobalStats.wastedHealing += healthCurrent - healthMax;
            #endregion
            healthCurrent = healthMax;
        }
        if (healthCurrent <= 0)
        {
            print (name + " Dead.");
            if (fDropItem != false)
            {
                fDropItem.DropItem();
            }
            gameObject.SetActive(false);

            #region GlobalStats
            GlobalStats.deaths += 1;       
            if (gameObject.layer == 9)
            {
                fLAT.Layer = "Player";
                if (gameObject.tag == "Player")
                {
                    fLAT.Tag = "Player";
                }
                GlobalStats.playerDeaths += 1;
            }
            else if (gameObject.layer == 10)
            {
                fLAT.Layer = "Enemy";
                GlobalStats.enemyDeaths += 1;
                if (gameObject.tag == "Ridden")
                {
                    fLAT.Tag = "Ridden";
                    GlobalStats.riddenDeaths += 1;
                }
                else if (gameObject.tag == "Plague Doctor")
                {
                    fLAT.Tag = "Plague Doctor";
                    GlobalStats.doctorDeaths += 1;
                }
                GlobalStats.enemiesAlive -= 1;
            }
            #endregion
        }
    }
}
