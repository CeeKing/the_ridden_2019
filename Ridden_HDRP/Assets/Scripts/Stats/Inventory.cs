﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour
{
    [Header("Inventory")]   //items the character has available
    public int potionHeal = 0;
    public int grenadeExplode = 0;
    [Header("Settings")]
    public bool canPickup = true;
    [Header("Global Stat Script")]
    public UnityEngine.GameObject GlobalStatHolder;
    private GlobalStats GlobalStats;

    // Start is called before the first frame update
    void Start()
    {
        //Assign the global stat script as a referencable item
        if (GameObject.FindGameObjectWithTag("GlobalStats") != null)
        {
            GlobalStatHolder = GameObject.FindGameObjectWithTag("GlobalStats");
            GlobalStats = GlobalStatHolder.GetComponent<GlobalStats>();
        }
        else
        {
            print("ERROR: Global Stat Script not assigned. Stats in this component will not be logged globally.");
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnCollisionEnter(Collision collision)
    {
        if (canPickup)
        {
            if (collision.collider.gameObject.layer == LayerMask.NameToLayer("Drop"))
            {
                Drop drop = collision.gameObject.GetComponent<Drop>();
                if (drop)
                {
                    potionHeal += drop.potionHealQuantity;
                    grenadeExplode += drop.grenadeExplodeQuantity;

                    //print pickups
                    if (drop.potionHealQuantity > 0)
                    {
                        print("Picked up " + drop.potionHealQuantity + " Healing Potions!");
                    }
                    if (drop.grenadeExplodeQuantity > 0)
                    {
                        print("Picked up " + drop.grenadeExplodeQuantity + " Exploding Grenades!");
                    }

                    #region Global Stats
                    GlobalStats.pickupsPickedUp += 1;
                    GlobalStats.PotionHealsPickedUp(drop.potionHealQuantity);
                    GlobalStats.GrenadeExplodesPickedUp(drop.grenadeExplodeQuantity);
                    #endregion
                }
                Destroy(collision.gameObject);
                GlobalStats.pickupsStillDropped -= 1;
            }
        }
    }
}
