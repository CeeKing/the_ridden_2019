﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatScript : MonoBehaviour
{
    [Header ("Stats")]  //basic stats of all characters using this script
    public float healthCurrent;
    public float healthMax;
    
    [Header("Function Scripts")]
    public FunctionDropItem fDropItem;

    [Header("Global Stat Script")]
    public UnityEngine.GameObject GlobalStatHolder;
    private GlobalStats GlobalStats;

    // Start is called before the first frame update
    void Start()
    {
        //Assign the global stat script as a referencable item
        if (GameObject.FindGameObjectWithTag("GlobalStats") != null)
        {
            GlobalStatHolder = GameObject.FindGameObjectWithTag("GlobalStats");
            GlobalStats = GlobalStatHolder.GetComponent<GlobalStats>();
        }
        else
        {
            print("ERROR: Global Stat Script not assigned. Stats in this component will not be logged globally.");
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (healthCurrent > healthMax)
        {
            healthCurrent = healthMax;
        }
        if (healthCurrent <= 0)
        {
            print (name + " Dead.");
            if (fDropItem != false)
            {
                fDropItem.DropItem();
            }
            gameObject.SetActive(false);
        }
    }
}