﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlobalStats : MonoBehaviour
{
    //This script keeps track of as many variables as possible since the start of the game.
    //It must be assigned as a reference by other scripts who will then send it their variables as they change.

    [Header("Spawns")]
    public int spawns = 0;
    public int playerSpawnCount = 0;
    public int enemySpawnCount = 0;
    public int riddenSpawnCount = 0;
    public int doctorSpawnCount = 0;
    public int enemiesAlive = 0;
    [Header("Deaths")]
    public int deaths = 0;
    public int enemyDeaths = 0;
    public int riddenDeaths = 0;
    public int doctorDeaths = 0;
    public int playerDeaths = 0;
    [Header("Combat")]
    public int hits = 0;
    public int playerHits = 0;
    public int enemyHits = 0;
    public int riddenHits = 0;
    public int doctorHits = 0;
    public float damageDealt = 0;
    public float playerDamageDealt = 0;
    public float enemyDamageDealt = 0;
    public float riddenDamageDealt = 0;
    public float doctorDamageDealt = 0;
    [Header("Item Drops")]
    public int pickupDropAttempts = 0;
    public int pickupsDropped = 0;
    public int pickupsStillDropped = 0;
    public int pickupsPickedUp = 0;
    public int potionHealsPickedUp = 0;
    public int grenadeExplodesPickedUp = 0;
    [Header("Misc.")]
    public float wastedHealing = 0;

    public void DamageDealt(float amount = 1)
    {
        damageDealt += amount;
    }
    public void PlayerDamageDealt(float amount = 1)
    {
        playerDamageDealt += amount;
    }
    public void EnemyDamageDealt(float amount = 1)
    {
        enemyDamageDealt += amount;
    }
    public void RiddenDamageDealt(float amount = 1)
    {
        riddenDamageDealt += amount;
    }
    public void DoctorDamageDealt(float amount = 1)
    {
        doctorDamageDealt += amount;
    }
    public void PotionHealsPickedUp(int amount = 1)
    {
        potionHealsPickedUp += amount;
    }
    public void GrenadeExplodesPickedUp(int amount = 1)
    {
        grenadeExplodesPickedUp += amount;
    }
}
