﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Throwable : MonoBehaviour
{
    public bool useArc;
    public int type;
    public float angle;
    public float throwForce;
    public Vector3 target;
    void Start()
    {
        transform.LookAt(target);

        if (useArc)
        {
            float dist = Vector3.Distance(transform.position, target);

            float angle = dist;
            transform.Rotate(angle, 0, 0, Space.Self);

            GetComponent<Rigidbody>().AddForce(transform.forward * throwForce);
        }
        else
        {
            GetComponent<Rigidbody>().freezeRotation = true;
            GetComponent<Rigidbody>().useGravity = false;
        }
    }

    void Update()
    {
        if (!useArc)
        {
            transform.Translate(Vector3.forward * Time.deltaTime * throwForce/100);
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        Destroy(gameObject);
    }
}
