﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine; 
using UnityEngine.Experimental.Rendering.HDPipeline;

public class DirLightCookieScript : MonoBehaviour
{
    public float speed;
    private Vector3 pos;
    public float scale;

    void Start()
    {
        pos = gameObject.transform.position;
    }

    void Update()
    {
        gameObject.transform.Translate(Vector3.right * Time.deltaTime * speed);

        if ( Vector3.Distance(gameObject.transform.position, pos) > scale)
        {
            gameObject.transform.position = pos;
        }
    }
}
