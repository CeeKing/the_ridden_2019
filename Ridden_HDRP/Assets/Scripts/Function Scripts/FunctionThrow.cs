﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FunctionThrow : MonoBehaviour
{
    public float throwForce;
    public bool useArc;
    public UnityEngine.GameObject[] grenades;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public bool Throw(Vector3 target, int type = 0)
    {
        if (type < grenades.Length)
        {
            UnityEngine.GameObject thrown = Instantiate(grenades[type], transform.position, transform.rotation);
            thrown.GetComponent<Throwable>().useArc = useArc;
            thrown.GetComponent<Throwable>().target = target;
            thrown.GetComponent<Throwable>().throwForce = throwForce;
            thrown.layer = gameObject.layer;
            return true;
        }
        else
        {
            print ("ERROR: No Grenade Type" + type + " Assigned.");
            return false;
        }
    }
}
