﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FunctionGetGlobalStatScript : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public GameObject GetGlobalStatScript()
    {
        GameObject GSS =  GameObject.FindGameObjectWithTag("GlobalStats");
        if (GSS != null)
        {
            return GSS;
        }
        else
        {
            return null;
        }
    }
}
