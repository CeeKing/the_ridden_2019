﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FunctionLayerAndTag : MonoBehaviour
{
    [Header("Info")]    //metadata about the character, mostly for debugging
    public string Layer;
    public string Tag;
    [Header("Global Stat Script")]
    public UnityEngine.GameObject GlobalStatHolder;
    private GlobalStats GlobalStats;
    // Start is called before the first frame update
    void Start()
    {
        //Assign the global stat script as a referencable item
        if (GameObject.FindGameObjectWithTag("GlobalStats") != null)
        {
            GlobalStatHolder = GameObject.FindGameObjectWithTag("GlobalStats");
            GlobalStats = GlobalStatHolder.GetComponent<GlobalStats>();
        }
        else
        {
            print("ERROR: Global Stat Script not assigned. Stats in this component will not be logged globally.");
        }

        #region Global Stats 
        GlobalStats.spawns += 1;       
        if (gameObject.layer == 9)
        {
            Layer = "Player";
            if (gameObject.tag == "Player")
            {
                Tag = "Player";
            }
            GlobalStats.playerSpawnCount += 1;
        }
        else if (gameObject.layer == 10)
        {
            Layer = "Enemy";
            GlobalStats.enemySpawnCount += 1;
            if (gameObject.tag == "Ridden")
            {
                Tag = "Ridden";
                GlobalStats.riddenSpawnCount += 1;
            }
            else if (gameObject.tag == "Plague Doctor")
            {
                Tag = "Plague Doctor";
                GlobalStats.doctorSpawnCount += 1;
            }
            GlobalStats.enemiesAlive += 1;
        }
        #endregion
    }

    public string GetLayerOpposite()
    {
        if (Layer == "Player")
        {
            return "Enemy";
        }
        else
        {
            return "Player";
        }
    }
}
