﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FunctionAttack : MonoBehaviour
{
    public float damage;
    public GameObject hitBox;
    [Header("Function Scripts")]
    public FunctionLayerAndTag fLAT;
    public FunctionHitbox fHitBox;
    [Header("Global Stat Script")]
    public GameObject GlobalStatHolder;
    private GlobalStats GlobalStats;
    // Start is called before the first frame update
    void Start()
    {
        //Assign the global stat script as a referencable item
        if (GameObject.FindGameObjectWithTag("GlobalStats") != null)
        {
            GlobalStatHolder = GameObject.FindGameObjectWithTag("GlobalStats");
            GlobalStats = GlobalStatHolder.GetComponent<GlobalStats>();
        }
        else
        {
            print("ERROR: Global Stat Script not assigned. Stats in this component will not be logged globally.");
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public bool Attack()    //Returns true or false depending on whether it hits something. Will return false if no hitbox is assigned.
    {   
        if (hitBox != null)
        {
            if (fHitBox.colliding.Count > 0)
            {
                bool didHit = false;
                foreach (GameObject hit in fHitBox.colliding)
                {
                    if (hit.layer == LayerMask.NameToLayer(fLAT.GetLayerOpposite()))
                    {
                        didHit = true;
                        float otherHealth = hit.gameObject.GetComponent<Health>().healthCurrent - damage;
                        hit.gameObject.GetComponent<Health>().healthCurrent = otherHealth;
                        
                        #region Global Stats
                        GlobalStats.hits += 1;
                        if (fLAT.Layer == "Player")
                        {
                            GlobalStats.playerHits += 1;
                        }
                        else if (fLAT.Layer == "Enemy")
                        {
                            if (fLAT.Tag == "Ridden")
                            {
                                GlobalStats.riddenHits += 1;
                            }
                            else if (fLAT.Tag == "PlagueDoctor")
                            {
                                GlobalStats.doctorHits += 1;
                            }
                        }

                        GlobalStats.DamageDealt(damage);
                        if (fLAT.Layer == "Player")
                        {
                            GlobalStats.PlayerDamageDealt(damage);
                        }
                        else if (fLAT.Layer == "Enemy")
                        {
                            GlobalStats.EnemyDamageDealt(damage);
                            if (fLAT.Tag == "Ridden")
                            {
                                GlobalStats.RiddenDamageDealt(damage);
                            }
                            else if (fLAT.Tag == "PlagueDoctor")
                            {
                                GlobalStats.DoctorDamageDealt(damage);
                            }
                        }
                        #endregion
                    }
                }
                return didHit;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }
}
