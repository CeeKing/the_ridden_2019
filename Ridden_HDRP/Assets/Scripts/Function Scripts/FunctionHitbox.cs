﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FunctionHitbox : MonoBehaviour
{
    [Tooltip("Gameobjects currently able to be hit by this hitbox")]
    public List<UnityEngine.GameObject> colliding = new List<UnityEngine.GameObject>();

    void OnTriggerEnter(Collider collider)
    {
        colliding.Add(collider.gameObject);
    }
    void OnTriggerExit(Collider collider)
    {
        colliding.Remove(collider.gameObject);
    }
}
