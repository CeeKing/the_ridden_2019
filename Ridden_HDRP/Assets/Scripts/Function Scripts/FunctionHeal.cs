﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FunctionHeal : MonoBehaviour
{
    [Tooltip("How many times per second the health incremetns when healing. Does not impact duration.")]
    public float ticksPerSecond;
    public bool healing;
    public int healsOverTimeRemaining = 0;
    private int amountDefault = 0;
    private float durationDefault = 0;
    public Health health;

    void LateUpdate()
    {
        if ((healsOverTimeRemaining > 0) && (!healing)) //this causes multiple heal over times to stack in duration, instead of stacking strength while their durations overlap.
        {
            StartCoroutine(HealOverTimeEnum(amountDefault, durationDefault));
        }
    }

    public bool HealOverTime(int amount, float duration)
    {
        if (true)   //make variable "canHeal" or something
        {   
            amountDefault = amount;
            durationDefault = duration;
            healsOverTimeRemaining += 1;
            return true;
        }
        else
        {
            //return false;
        }
    }

    private IEnumerator HealOverTimeEnum(int amount, float duration)
    {
        healing = true;
        for (float i = 0; i < amount; i += amount/duration*ticksPerSecond)
        {
            health.healthCurrent += amount / (duration * ticksPerSecond);
            yield return new WaitForSeconds(1/ticksPerSecond);
        }
        healsOverTimeRemaining -= 1;
        healing = false;
    }

    public bool HealInstant(int amount)
    {
        if (true)   //make variable "canHeal" or something
        {
            health.healthCurrent += amount;
            return true;
        }
        else
        {
            //return false;
        }
    }

}
