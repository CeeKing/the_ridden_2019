﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FunctionDropItem : MonoBehaviour
{
    [Tooltip("Chance to drop an item as a % (0-100)")]
    public float dropChance;
    public UnityEngine.GameObject drop;
    public UnityEngine.GameObject[] items;
    public float[] chances;
    [Header("Global Stat Script")]
    public UnityEngine.GameObject GlobalStatHolder;
    private GlobalStats GlobalStats;
    // Start is called before the first frame update
    void Start()
    {
        //Assign the global stat script as a referencable item
        if (GameObject.FindGameObjectWithTag("GlobalStats") != null)
        {
            GlobalStatHolder = GameObject.FindGameObjectWithTag("GlobalStats");
            GlobalStats = GlobalStatHolder.GetComponent<GlobalStats>();
        }
        else
        {
            print("ERROR: Global Stat Script not assigned. Stats in this component will not be logged globally.");
        }
    }
    public void DropItem()
    {
        float ran = Random.Range(0f, 100f);
        GlobalStats.pickupDropAttempts += 1;
        //print ("Rolled " + (100-ran) + "/" + (100-dropChance));
        if ( ran <= dropChance)
        {
            //drop only one item
            //total chance of all drops should not exceed 100
            ran = Random.Range(0f, 100f);
            //print ("Rolled " + (100-ran) + "/" + (100-dropChance));
            for (int i = 0; i < chances.Length; i++)
            {
                if (ran < chances[i])
                {
                    UnityEngine.GameObject newDrop = Instantiate(items[i], transform.position, transform.rotation);
                    GlobalStats.pickupsDropped += 1;
                    GlobalStats.pickupsStillDropped += 1;
                    print ("Dropped: " + items[i].name);
                    break;    
                }
                else
                {
                    ran -= chances[i];
                }
            }
        }
    }
}
