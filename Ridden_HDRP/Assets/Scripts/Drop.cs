﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Drop : MonoBehaviour
{
    [Header("Spawn Motion")]
    public float upPush;
    public float sidePush;
    [Header("Quantity of Items to Give")]
    public int potionHealQuantity = 0;
    public int grenadeExplodeQuantity = 0;
    [Header("Other")]
    public Rigidbody rb;
    [Header("Global Stat Script")]
    public UnityEngine.GameObject GlobalStatHolder;
    private GlobalStats GlobalStats;
    // Start is called before the first frame update
    void Start()
    {
        //Assign the global stat script as a referencable item
        if (GameObject.FindGameObjectWithTag("GlobalStats") != null)
        {
            GlobalStatHolder = GameObject.FindGameObjectWithTag("GlobalStats");
            GlobalStats = GlobalStatHolder.GetComponent<GlobalStats>();
        }
        else
        {
            print("ERROR: Global Stat Script not assigned. Stats in this component will not be logged globally.");
        }

        if (gameObject.GetComponent<Rigidbody>() != null)
        {
            rb = gameObject.GetComponent<Rigidbody>();
        }
    }

    void OnEnable()
    {
        //apply the upwards direction force
        rb.AddForce(transform.up * upPush);

        rb.AddForce(transform.forward * ((Random.Range(-1f, 1f)) * sidePush));
        rb.AddForce(transform.right * ((Random.Range(-1f, 1f)) * sidePush));
    }
}
