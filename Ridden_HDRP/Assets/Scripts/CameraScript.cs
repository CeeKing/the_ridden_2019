﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScript : MonoBehaviour
{
    public float speed;

    private UnityEngine.GameObject player;
    private Vector3 boom;

    // Start is called before the first frame update
    void Start()
    {
        player = UnityEngine.GameObject.FindWithTag("Player");
        boom = transform.position - player.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 targetPos = player.transform.position + boom;
        transform.position = Vector3.Lerp(transform.position, targetPos, speed * Time.deltaTime);
    }
}
